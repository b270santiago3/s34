const express = require("express");

// This creates an express application and stores this in a constant called app
// This is our server
const app = express();

const port = 3000;


// Middleware
//
app.use(express.json());


// Allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
// Express has methods corresponding to each HTTP method

// This route expects to receive the GET request at the base URI "/"
// This route will return a simple message back to the client
app.get("/", (req, res) => {

	// res.send uses the Express.JS module's method to send a response back to the client
	res.send("Hello World!")
})

// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res) => {

		res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})


// mock database
let users = [];

app.post("/signup", (req,res) => {
	console.log(req.body)

	if(req.body.usernane !== "" && req.body.password !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} is successfully registered`)
	} else {
		res.send('Please inpute BOTH username and password.')
	}
})

app.put("/change-password", (req, res) => {
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++){

		// Changes the password of the user found by the loop into the password provided in the client
		if (req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`
				;
			break

		// If no user was found
		} else {

			message = `User does not exist.`;
		}

	}
	res.send(message);
})


// ACTIVITY


app.get('/home', (req, res) => {
  	res.send('Welcome to the home page');
});

app.get('/users', (req, res) => {
   	const allUsers = users;
   	res.send(allUsers);	 
});


app.delete('/delete-user', (req, res) => {
	let message;

	for (let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username} has been deleted.`
				;
			break

		} else {

			message = `User does not exist.`;
		}

	}
	res.send(message);
})

  	
app.listen(port, () => console.log(`Server is running at port ${port}`));

